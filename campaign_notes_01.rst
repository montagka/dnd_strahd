=========
Session 1
=========


Gnarlut believes that all the things that happened at the death house was a dream.

Interested in dealing with the hags.

* Hags are much more fucked up than we originally thought. Much more animal than person.
* I found Green Hags.

We know that Lanny is no longer on this plane.

The board of opportunity.

Reports of werewolves attacking fringe farms. Missing group went out to find her. Katherine missing. Zessica wants us to investigate.

Tom
===

* Can't find the wolves. They just come and go. Came from the Westerwoods to the NW. Might be able to hunt them down.


After chasing off the werewolves from Jan Vuurst, the fog rolls in. We then find a path leading off to a large stone wall:

.. image:: first_stone_wall.jpg

Feels familiar, but I can't place it. 

We enter the gates, which opened on their own for us, and attempt to make camp. Entering the woods gives us a level of exhaustion. We are unable to get to sleep. Instead, we continue along the trail and come to a large clearing in the forest.

The Village of Barovia.
=======================

* We enter the town, and there is a gnawing sense of familiarity around one of the houses...
* There is an old lady who is buying a child from a couple. 
* She informs us we are in the land of Barovia. She sells pastries in exchange for children.
* We get the child back and give him back to the parentals.
    * We go to the tavern: "Blood on the Vine Tavern"


Blood of the Vine Tavern
------------------------

* Loud raucous group of people

